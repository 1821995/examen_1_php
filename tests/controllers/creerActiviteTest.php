<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    private const NOM_CHAINE_VALIDE="« Nathalie, Jean-Marc et ‘tite laine»";
    private const NOM_CHAINE_INVALIDE= "« 33 Cité-des-Jeunes »";

    static function setUpBeforeClass(): void
    {
        $_SESSION = array();
    }
    //@TODO implémenter ce test
    public function testEstValideChaineValide():void
    {
        $this->assertTrue(true, self::NOM_CHAINE_VALIDE);

    }

    //@TODO implémenter ce test
    public function testEstValideChaineInValide():void
    {
        $this->assertTrue(false, self::NOM_CHAINE_INVALIDE);
    }
    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
