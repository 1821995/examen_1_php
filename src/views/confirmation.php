<?php
//VL Ne pas oublier de démarrer la session pour l'utiliser
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once ("../controllers/creerActivite.php");
include("templates/header.php")
?>
<main class="container-md">
    <!-- Fil d'ariane -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="#">Journal</a>
            <li class="breadcrumb-item"><a href="#">Cours 1</a></li>
            <li class="breadcrumb-item"><a href="semaine.php">Semaine 1</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter une activité</li>
        </ol>
    </nav>

    <?php
        //@TODO : appeller le contrôleur pour ajouter l'activité
        // VL à retirer : déjà inclu plus haut
        require_once("../../src/controllers/creerActivite.php");
        // VL si le formulaire a été envoyé, l'activité ne sera pas ajoutée
        // if(!isset($_POST["activite"])){
           if(isset($_POST["activite"])){
                // création d'une nouvelle activité s'Il y en a pas
                $_SESSION["membre"]= $_POST["activite"];
                $activite= creerActivite::ajoutNouvelleActivite($_SESSION["membre"]);
                if($activite){
                    echo '<p class="display-4 mt-4">L\'activité a été ajoutée à votre journal</p>';
                }
            }
    ?>
    <a class="btn btn-primary mt-4" href="ajoutActivite.php">Ajouter une autre activité</a>
</main>
<?php include("templates/footer.php") ?>