<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

class creerActivite
{
    private static function getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    /*TODO Compléter la méthode de validation
    Critères de validation :
    - Accepter lettres, ",", ";" "-" "'" pour les champs texte */
    private static function estValide(string $chaine): bool
    {
        // VL À revoir; je te suggère de tester avec https://regex101.com/
        $patternNom= '/^\D[a-z]$/'; //@TODO vérifie que le nom commence par une majuscule et contient 4 à 8 caractères aphabétiques
        if (preg_match($patternNom, $chaine)){
            return  true;
        }else{
            return false;
        }
    }


    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide
        //foreach ($details as $detail) {
            //if (self::estValide($detail)) { //VL bien sauf qu'il faut envoyer seulement quelques propriétés (ex pas les nombres)
                // si valide, créer l'activité
        $activite = new Activite($details);
            //}


            //@TODO Si valide, l'ajouter au tableau des activités
        //VL self::getActivites() retourne un tableau et non un objet
        $creation=self::getActivites()->addActivite($activite);  // ajout de l'activité avec la méthode addActivite de la classe Activite


        if($creation){
            return true;
        }else{
           return false;
        }
        //Retourner vrai si l'activité a été ajoutée, faux sinon
       // return $creation;
    }
}