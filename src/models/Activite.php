<?php


class Activite
{

    /*
     * propriétés de la classe activité
     * **/
    private string $date;
    private string $activites;
    private int $intensite;
    private string $determinants;
    private int $duree;
    private array $details;
    private array $Lesactivites;


    /**
     * Activite constructor.
     * @param array $details
     */
    public function __construct(array $details)
    {
        foreach ($details as $detail => $valeur) {
            if (property_exists($this, $detail)) {
                $this->$detail = $valeur;
            }

        }
    }

    /**
     * permet d'ajouter une activité à la liste d'activités
     * @param Activite $activite
     */
    public function addActivite(Activite $activite):void
    {
        array_push( $this->Lesactivites, $activite);

    }



    // getters et setters des propriétés
    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }
    /**
     * @return string
     */
    public function getActivites(): string
    {
        return $this->activites;
    }

    /**
     * @param string $activites
     */
    public function setActivites(string $activites): void
    {
        $this->activites = $activites;
    }
    /**
     * @return int
     */
    public function getIntensite(): int
    {
        return $this->intensite;
    }

    /**
     * @param int $intensite
     */
    public function setIntensite(int $intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * @return int
     */
    public function getDuree(): int
    {
        return $this->duree;
    }

    /**
     * @param int $duree
     */
    public function setDuree(int $duree): void
    {
        $this->duree = $duree;
    }

    /**
     * @return array
     */
    public function getDeterminants(): string
    {
        return $this->determinants;
    }

    /**
     * @param array $determinants
     */
    public function setDeterminants(string $determinants): void
    {
        $this->determinants = $determinants;
    }

    /**
     * @return array
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * @param array $details
     */
    public function setDetails(array $details): void
    {
        $this->details = $details;
    }

    /**
     * @return array
     */
    public function getLesactivites(): array
    {
        return $this->Lesactivites;
    }

    /**
     * @param array $Lesactivites
     */
    public function setLesactivites(array $Lesactivites): void
    {
        $this->Lesactivites = $Lesactivites;
    }
}